<?php


class Friend{
  $isAlive = true;
  $name;
  $houseColor;
  $profession;
  $hobby;
  $age;

  public function __construct($name, $houseColor, $profession, $hobby, $age){
    $this -> name=$name;
    $this -> houseColor=$houseColor;
    $this -> profession=$profession;
    $this -> hobby=$hobby;
    $this -> age=$age;
  }

  public function introduction() {
    return "Good to meetcha! My name is " . $this->name . "! I live in a " . $this->houseColor . " house, and I like to " . $this->hobby ".";
  }


}



?>
